# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# molnars <szabolcs.molnar@gmail.com>, 2025
# Csaba Tarjányi, 2025
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-07 07:29+0000\n"
"PO-Revision-Date: 2025-01-20 12:34+0000\n"
"Last-Translator: Csaba Tarjányi, 2025\n"
"Language-Team: Hungarian (https://app.transifex.com/rosarior/teams/13584/hu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:24 events.py:6 permissions.py:6 settings.py:12
msgid "Logging"
msgstr "Naplózás"

#: apps.py:40
msgid "System"
msgstr "Rendszer"

#: apps.py:64
msgid "Domain"
msgstr ""

#: events.py:10
msgid "Error log deleted"
msgstr ""

#: links.py:15 views.py:36
msgid "Global error log"
msgstr ""

#: links.py:26
msgid "Delete"
msgstr "Törlés"

#: links.py:31
msgid "Errors"
msgstr ""

#: links.py:37
msgid "Clear errors"
msgstr "Hibák törlése"

#: model_mixins.py:11
#, python-format
msgid "Unknown domain name: %s"
msgstr ""

#: model_mixins.py:18
msgid "Object"
msgstr "Objektum"

#: model_mixins.py:26
msgid "App label"
msgstr "Alkalmazáscímke"

#: models.py:21 models.py:41
msgid "Internal name"
msgstr "Belső név"

#: models.py:26 models.py:38
msgid "Error log"
msgstr ""

#: models.py:27
msgid "Error logs"
msgstr ""

#: models.py:55 models.py:69
msgid "Error log partition"
msgstr ""

#: models.py:56
msgid "Error log partitions"
msgstr ""

#: models.py:73
msgid "Date and time"
msgstr "Dátum és idő"

#: models.py:78
msgid "Domain name"
msgstr ""

#: models.py:81
msgid "Text"
msgstr "Szöveg"

#: models.py:89
msgid "Error log partition entry"
msgstr "Hibanapló-partíció bejegyzés"

#: models.py:90
msgid "Error log partition entries"
msgstr ""

#: permissions.py:10
msgid "Delete error log"
msgstr ""

#: permissions.py:13
msgid "View error log"
msgstr ""

#: serializers.py:12
msgid "Content type"
msgstr "Tartalom típus"

#: serializers.py:16
msgid "Object ID"
msgstr "Objektum azonosító"

#: serializers.py:19
msgid "URL"
msgstr "URL"

#: settings.py:18
msgid "Disable logging message ANSI color codes."
msgstr "Az üzenetek ANSI színkódos naplózásának letiltása."

#: settings.py:24
msgid "Automatically enable logging to all apps."
msgstr ""

#: settings.py:30
msgid "List of handlers to which logging messages will be sent."
msgstr ""

#: settings.py:35
msgid "Level for the logging system."
msgstr ""

#: settings.py:41
msgid "Path to the logfile that will track errors during production."
msgstr ""
"A naplófájl elérési útja, amely nyomon követi a hibákat az üzemeltetés "
"során."

#: views.py:30
msgid ""
"This view displays the error log of different objects. An empty list is a "
"good thing."
msgstr ""

#: views.py:34 views.py:99
msgid "There are no error log entries"
msgstr "Nincsenek hibanapló bejegyzések"

#: views.py:50
#, python-format
msgid "Clear error log entries for: %s"
msgstr "Hibanapló bejegyzések törlése a következőhöz: %s"

#: views.py:57
msgid "Object error log cleared successfully"
msgstr "Az objektum hibanapló sikeresen törölve"

#: views.py:74
#, python-format
msgid "Delete error log entry: %s"
msgstr "Hibanapló bejegyzés törlése: %s"

#: views.py:95
msgid ""
"This view displays the error log of an object. An empty list is a good "
"thing."
msgstr ""

#: views.py:103
#, python-format
msgid "Error log entries for: %s"
msgstr "Hibanapló bejegyzések a következőhöz: %s"
